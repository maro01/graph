#ifndef C_ARC_H
#define C_ARC_H

#include <iostream>

class CArc
{
private:
	int nADestination;


public:

	 CArc();

	 CArc(int nDes);

	 CArc(CArc* pAArg);

	int AGetDestination();

	void ASetDestination(int nArg);

};
#include "CArc.cpp"
#endif
