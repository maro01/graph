#ifndef C_EXCEPTION_H
#define C_EXCEPTION_H

#include <iostream>

class CException
{
private:
	int eECodeErreur;


public:
	 CException();

	 CException(CException* pEArg);

	virtual void EAfficherErreur() = 0;

	int EGetCodeErreur(); 

	void EModifierCodeErreur(int EArg);

};
#include "CException.cpp"
#endif
