#ifndef C_EXCEPTION_FILE_SYNTAX_H
#define C_EXCEPTION_FILE_SYNTAX_H

#include <iostream>

#include "CException.h"

class CExceptionFileSyntax : public CException
{
private:
	char* pcEFSMessage;


public:
	 CExceptionFileSyntax();

	 CExceptionFileSyntax(CExceptionFileSyntax* pEFSArg);

	 ~CExceptionFileSyntax();

	void EAfficherErreur();

	void EFSSetMessage(char* pcMessage);

	char* EFSGetMessage();

};
#include "CExceptionFileSyntax.cpp"
#endif
