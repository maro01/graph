#ifndef C_EXCEPTION_FILE_NOT_FOUND_H
#define C_EXCEPTION_FILE_NOT_FOUND_H

#include <iostream>

#include "CException.h"

class CExceptionFileNotFound : public CException
{
private:
	char* pcEFNFMessage;


public:
	 CExceptionFileNotFound();

	 CExceptionFileNotFound(CExceptionFileNotFound* pFNFArg);

	 ~CExceptionFileNotFound();

	void EAfficherErreur();

	void EFNFSetMessage(char* pcMessage);

	char* EFNFGetMessage();

};
#include "CExceptionFileNotFound.cpp"
#endif
