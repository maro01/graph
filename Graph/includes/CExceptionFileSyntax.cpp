
/**
@brief Constructeur par defaut d'objet CExceptionFileSyntax
*/
CExceptionFileSyntax::CExceptionFileSyntax() : CException()
{
	pcEFSMessage = (char*)malloc(sizeof(char) * strlen("CExceptionFileSyntax raised") + 1);
	strcpy_s(pcEFSMessage, sizeof("CExceptionFileSyntax raised"), "CExceptionFileSyntax raised");
}
/**
@brief Constructeur de copie d'objet pointeur CExceptionFileSyntax*
@param[in] Objet pointeur CExceptionFileSyntax*
*/
CExceptionFileSyntax::CExceptionFileSyntax(CExceptionFileSyntax* pEFSArg) : CException(pEFSArg)
{
	pcEFSMessage = pEFSArg->EFSGetMessage();
}
/**
@brief Destructeur d'objet CExceptionFileSyntax
*/
CExceptionFileSyntax::~CExceptionFileSyntax()
{
	free(pcEFSMessage);
}
/**
@brief Affichage d'erreur d'objet CExceptionFileSyntax
@param[in] -
@param[out] -
*/
void CExceptionFileSyntax::EAfficherErreur()
{
	if (EGetCodeErreur() == 1) std::cerr<<"\n\n"<<pcEFSMessage<<"\n\n";
	else std::cerr<<"\n\nCExceptionFileSyntax not raised\n\n";
}
/**
@brief Setter du message d'erreur d'objet CExceptionFileSyntax
@param[in]  Pointeur de chaine de caracteres
@param[out] -
*/
void CExceptionFileSyntax::EFSSetMessage(char* pcMessage)
{
	strcpy_s(pcEFSMessage, sizeof(pcMessage), pcMessage);
}
/**
@brief Getter du message d'erreur d'objet CExceptionFileSyntax
@param[in]  -
@param[out] -
*/
char * CExceptionFileSyntax::EFSGetMessage()
{
	return nullptr;
}