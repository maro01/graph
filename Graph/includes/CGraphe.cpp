
/**
@brief contructeur CGraphe par defaut
*/
CGraphe::CGraphe()
{
	nGTaille = 0;
	pSListeSommets = NULL;
}

/**
@brief contructeur CGraphe de copie
@param[in] Objet pointeur de type CGraphe
*/
CGraphe::CGraphe(CGraphe * pGArg)
{
	if (!GIdSommetsRepetes(pGArg->GGetListeSommets(), pGArg->GGetTaille()))
	{
		nGTaille = pGArg->nGTaille;
		pSListeSommets = new CSommet*[nGTaille];
		for (int i = 0; i < nGTaille; ++i)  pSListeSommets[i] = new CSommet(pGArg->pSListeSommets[i]);
	}
	else
	{
	CExceptionDuplicatedSommetId* EDSObjet = new CExceptionDuplicatedSommetId();
	EDSObjet->EModifierCodeErreur(1);
	throw(EDSObjet);
	}
}
/**
@brief destructeur CGraphe
*/
CGraphe::~CGraphe()
{
	for (int i = 0; i < nGTaille; i++) {
		free(pSListeSommets[i]);
	}
	free(pSListeSommets);
}
/**
@brief Setter pour la liste des sommets d'objet CGraphe
@param[in] Objet pointeur 2D de type CSommet
@param[in] Entier pour la taille de la liste des sommets entree
@param[out] -
*/
void CGraphe::GSetListeSommets(CSommet ** pSArg, int nTaille)
{
	if (!GIdSommetsRepetes(pSArg, nTaille))
	{
		nGTaille = nTaille;
		pSListeSommets = new CSommet*[nGTaille];
		for (int i = 0; i < nGTaille; ++i) {
			pSListeSommets[i] = new CSommet(pSArg[i]);
		}
	}
	else
	{
		CExceptionDuplicatedSommetId* EDSObjet = new CExceptionDuplicatedSommetId();
		EDSObjet->EModifierCodeErreur(1);
		throw(EDSObjet);
	}
	
}
/**
@brief Getter pour recuperer liste des sommets d'objet CGraphe
@param[out] Objet pointeur 2D de type CSommet
*/
CSommet ** CGraphe::GGetListeSommets()
{
	return pSListeSommets;
}
/**
@brief Ajout d'un nouveau sommet � la liste des sommets d'objet CGraphe
@param[in] Objet pointeur de type CSommet*
@param[out] -
*/
void CGraphe::GAddSommet(CSommet * pSArg)
{
	if(pSArg)
	nGTaille++;
	if (nGTaille == 1)  pSListeSommets = new CSommet*();

	else pSListeSommets = (CSommet**)realloc(pSListeSommets, nGTaille * sizeof(CSommet*));

	pSListeSommets[nGTaille - 1] = new CSommet(pSArg);
}
/**
@brief Getter pour la taille de l'objet CGraphe
@param[out] Entier 
*/
int CGraphe::GGetTaille()
{
	return nGTaille;
}
/**
@brief Suppression d'un sommet de la liste des sommets d'objet CGraphe en fonction de l'index saisi en argument
@param[in] Entier pour l'index de l'objet CSommet en question
@param[out] -
*/
void CGraphe::GDeleteSommet(int nNumero)
{
	if (nGTaille > 0)
	{
		if (pSListeSommets[0]->SGetNumero() == nNumero)
		{
			if(nGTaille>1) for (int i = 0; i < nGTaille; i++) pSListeSommets[i] = pSListeSommets[i + 1];
		}
		else
		{
			for (int i = 0; i < nGTaille; i++)
			{
				if (pSListeSommets[i]->SGetNumero() == nNumero)
				{
					if (i < (nGTaille - 1))
					{
						for (int j = i; j < nGTaille - 1; j++)
						{
							pSListeSommets[j] = pSListeSommets[j + 1];
						}
					}
				}

			}
		}
		--nGTaille;
		pSListeSommets = (CSommet**)realloc(pSListeSommets, nGTaille * sizeof(CSommet*));

		for (int i = 0; i < nGTaille; i++)
		{
			pSListeSommets[i]->SSupprimerPartantArrivant(nNumero);
		}
	}
	else std::cout << "Suppression dans liste partant invalide" << std::endl << std::endl;
}
/**
@brief Remplacemet un sommet de la liste des sommets d'objet CGraphe par un autre selon l'index du CSommet saisi en argument
@param[in] Entier pour l'index de l'objet CSommet en question
@param[in] Objet pointeur de type CSommet* qui va remplacer un autre CSommet selon l'index saisi sur la liste de CSommets du CGraphe
@param[out] -
*/
void CGraphe::GRemplacerSommet(int nNumero, CSommet * pSNewSommet)
{
	for (int i = 0; i < nGTaille; i++)
		if (pSListeSommets[i]->SGetNumero() == nNumero) pSListeSommets[i] = new CSommet(pSNewSommet);
}

/**
@brief Affichage du graphe 
*/
void CGraphe::GAfficherGraphe()
{
	if (nGTaille == 0) std::cout << " Graphe vide " << std::endl << std::endl;
	else
	{
		std::cout << "Graphe-----------------------------------------------" << std::endl<<std::endl;
		std::cout << "Liste des sommets : " << std::endl << std::endl;
		for (int i = 0; i < nGTaille; i++)
		{
		     std::cout<<pSListeSommets[i]->SGetNumero()<< " - ";
		}
		std::cout << std::endl << std::endl;
		std::cout << "Liste des arcs : " << std::endl << std::endl;
		for (int i = 0; i < nGTaille; i++)
		{
			pSListeSommets[i]->SAfficherArcs();
		}
	}
}
/**
@brief Chercher dans la liste des sommets si l'index saisi en argument represente deja un sommet dans la liste des sommets.
@param[in] Entier pour l'index en question
@param[out] valeur booleenne indiquant si le sommet est deja existant dans la liste des sommets  ou non
*/
bool CGraphe::GEstUnSommetExistant(int nIdSommet)
{
	for (int i = 0; i < nGTaille; i++)
	{
		if (pSListeSommets[i]->SGetNumero() == nIdSommet) return true;
	}
	return false;
}

/**
@brief Chercher s'il existe des Id de sommets repetes dans une liste de sommets saisie en argument
@param[in] Liste d'objets CSommet 
@param[in] Entier pour la taille de la liste d'objets CSommet en question
@param[out] Valeur booleenne indiquant si la liste contient des id de sommets redondants
*/
bool CGraphe::GIdSommetsRepetes(CSommet** pSArg,int nTaille)
{
	int nId = 0;
	for (int i = 0; i < nTaille; i++)
	{
		nId = pSArg[i]->SGetNumero();
		for (int j = 0; j < nTaille; j++)
		{
			if (nId == pSArg[j]->SGetNumero() && j != i) return true;
		}
	}
	return false;
}

/**
@brief cherche le sommet ayant comme numero l'id passé en paramètre et le renvoie
@param[in] Numero du CSommet à retourner
@param[out] pointeur sur le CSommet à rechercher
*/
CSommet* CGraphe::GGetSommet(int nIdSommet)
{
	if(GEstUnSommetExistant(nIdSommet))
	{
		for(int nBoucle = 0; nBoucle < nGTaille; nBoucle++)
		{
			if(pSListeSommets[nBoucle]->SGetNumero() == nIdSommet)
			{
				return pSListeSommets[nBoucle];
			}
		}
	}
	return nullptr;
}

/**
@brief Chercher s'il existe des Id de sommets repetes dans une liste de sommets saisie en argument
@param[out] retourne un pointeur sur un CGraphe ayant tout les arcs inverse
*/
CGraphe* CGraphe::GGrapheInverse()
{
	CGraphe* GgraphReturn = new CGraphe();

	for(int nBoucle = 0; nBoucle < nGTaille; nBoucle++)
	{
		CSommet *pSsommet = new CSommet(pSListeSommets[nBoucle]);

		pSsommet->SSetListeArrivant(pSListeSommets[nBoucle]->SGetListePartant(), pSListeSommets[nBoucle]->SGetTaillePartant());
		pSsommet->SSetListePartant(pSListeSommets[nBoucle]->SGetListeArrivant(), pSListeSommets[nBoucle]->SGetTailleArrivant());

		GgraphReturn->GAddSommet(pSsommet);
	}

	return GgraphReturn;
}