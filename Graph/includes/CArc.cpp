/**
@brief Constructeur par defaut d'objet CArc
*/
CArc::CArc()
{
	nADestination = 0;
}
/**
@brief contructeur par defaut d'objet CArc
@param[in] entier pour Id de sommet de destination
*/
CArc::CArc(int nDes)
{
	nADestination = nDes;
}
/**
@brief contructeur CArc de copie
@param[in] Objet pointeur de type CArc*
*/
CArc::CArc(CArc * pAArg)
{
	nADestination = pAArg->nADestination;
}
/**
@brief Getter de destination de l'objet CArc
@param[in] -
@param[out] entier (Id de sommet de destination de l'arc courant)
*/
int CArc::AGetDestination()
{
	return nADestination;
}

/**
@brief Setter de destination de l'objet CArc
@param[in] entier (Id de sommet de destination de l'arc courant)
@param[out] -
*/
void CArc::ASetDestination(int nArg)
{
	nADestination = nArg;
}
