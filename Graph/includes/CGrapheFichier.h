#ifndef C_GRAPHE_FICHIER_H
#define C_GRAPHE_FICHIER_H

#include <iostream>
#include "CGrapheFichier.h"
#include "CExceptionFileNotFound.h"
#include "CExceptionFileSyntax.h"
#include <fstream>

class CGrapheFichier
{
private:
	char* pcGFFilePath;

public:
	 CGrapheFichier();

	 CGrapheFichier(CGrapheFichier* pGFArg);

	 CGrapheFichier(char* pcArg);

	 ~CGrapheFichier();

	void GFSetFilePath(char* pcArg);

	char* GFGetFilePath();

	CGraphe* GFGetGraphe();

};
#include "CGrapheFichier.cpp"
#endif
