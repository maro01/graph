/**
@brief Constructeur par defaut d'objet CException
*/
CException::CException() : eECodeErreur(0) {}

/**
@brief Constructeur de copie d'objet pointeur CException
@param[in] Objet pointeur CException*
*/
CException::CException(CException* pEArg) : eECodeErreur(pEArg->EGetCodeErreur()) {}

/**
@brief Getter du code d'erreur (valeur entiere)
@param[in] -
@param[out] entier (1 ou 0)
*/
int CException::EGetCodeErreur()
{
	return eECodeErreur;
}

/**
@brief Modifier le code d'erreur (valeur entiere)
@param[in] entier (1 ou 0)
@param[out] -
*/
void CException::EModifierCodeErreur(int eArg)
{
	eECodeErreur = eArg;
}







