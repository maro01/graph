
/**
@brief Constructeur par defaut d'objet CExceptionDuplicatedSommetId
*/
CExceptionDuplicatedSommetId::CExceptionDuplicatedSommetId()
{
	pcEDSMessage = (char*)malloc(sizeof(char) * strlen("CExceptionDuplicatedSommetId raised") + 1);
	strcpy_s(pcEDSMessage, sizeof("CExceptionDuplicatedSommetId raised"), "CExceptionDuplicatedSommetId raised");
}
/**
@brief Constructeur de copie d'objet pointeur CExceptionDuplicatedSommetId*
@param[in] Objet pointeur CExceptionDuplicatedSommetId*
*/
CExceptionDuplicatedSommetId::CExceptionDuplicatedSommetId(CExceptionDuplicatedSommetId * pEDSIArg)
{
	pcEDSMessage = pEDSIArg->EDSIGetMessage();
}
/**
@brief Destructeur d'objet CExceptionDuplicatedSommetId
*/
CExceptionDuplicatedSommetId::~CExceptionDuplicatedSommetId()
{
	free(pcEDSMessage);
}
/**
@brief Affichage d'erreur d'objet CExceptionDuplicatedSommetId
@param[in] -
@param[out] -
*/
void CExceptionDuplicatedSommetId::EAfficherErreur()
{
	if (EGetCodeErreur() == 1) std::cerr << "\n\n" << pcEDSMessage << "\n\n";
	else std::cerr << "\n\nCExceptionFileNotFound not raised\n\n";
}
/**
@brief Setter du message d'erreur d'objet CExceptionDuplicatedSommetId
@param[in]  Pointeur de chaine de caracteres
@param[out] -
*/
void CExceptionDuplicatedSommetId::EDSISetMessage(char * pcMessage)
{
	strcpy_s(pcEDSMessage, sizeof(pcMessage), pcMessage);
}
/**
@brief Getter du message d'erreur d'objet CExceptionDuplicatedSommetId
@param[in]  -
@param[out] -
*/
char * CExceptionDuplicatedSommetId::EDSIGetMessage()
{
	return pcEDSMessage;
}
