
/**
@brief contructeur CSommet par defaut
*/
CSommet::CSommet()
{
	nSNumero = ++idSommet;
	pASArrivant = NULL;
	pASPartant = NULL;
	nSTailleArrivant = 0;
	nSTaillePartant = 0;
}
/**
@brief contructeur CSommet de copie
@param[in] Objet pointeur de type CSommet
*/
CSommet::CSommet(CSommet * pSArg)
{
	nSNumero = pSArg->nSNumero;
	nSTailleArrivant = pSArg->nSTailleArrivant;
	nSTaillePartant = pSArg->nSTaillePartant;

	pASPartant = (CArc**)realloc(pASPartant, nSTaillePartant * sizeof(CArc*));

	if (nSTaillePartant > 0)
	{
		pASPartant = new CArc*[nSTaillePartant];
		for (int i = 0; i < nSTaillePartant; ++i) {
			pASPartant[i] = new CArc(pSArg->pASPartant[i]);
		}
	}
	else pASPartant = NULL;

	pASArrivant = (CArc**)realloc(pASArrivant, nSTailleArrivant * sizeof(CArc*));

	if (nSTailleArrivant > 0)
	{
		pASArrivant = new CArc*[nSTailleArrivant];
		for (int i = 0; i < nSTailleArrivant; ++i) {
			pASArrivant[i] = new CArc(pSArg->pASArrivant[i]);
		}
	}
	else pASArrivant = NULL;

}
/**
@brief destructeur CSommet
*/
CSommet::~CSommet()
{
	if (nSTailleArrivant > 0)
	{
		for (int i = 0; i < nSTailleArrivant; i++) {
			free(pASArrivant[i]);
		}
		free(pASArrivant);
	}

	if (nSTaillePartant > 0)
	{
		for (int i = 0; i < nSTaillePartant; i++) {
			free(pASPartant[i]);
		}
		free(pASPartant);
	}

}
/**
@brief Getter pour recuperer l'ID du sommet courant
@param[in] -
@param[out] entier ( ID du sommet courant )
*/
int CSommet::SGetNumero()
{
	return nSNumero;
}
/**
@brief Setter pour l'ID du sommet courant
@param[in] entier ( ID du sommet courant )
@param[out] -
*/
void CSommet::SSetNumero(int nArg)
{
	nSNumero = nArg;
}
/**
@brief Setter pour la taille de la liste des arc arrivants au sommet courant 
@param[in] entier ( Taille de la liste des arc arrivants au sommet courant )
@param[out] -
*/
void CSommet::SSetTailleArrivant(int nArg)
{
	nSTailleArrivant = nArg;
}
/**
@brief Getter pour la taille de la liste des arc arrivants au sommet courant
@param[in]  -
@param[out] entier ( Taille de la liste des arc arrivants au sommet courant )
*/
int CSommet::SGetTailleArrivant()
{
	return nSTailleArrivant;
}
/**
@brief Setter pour la taille de la liste des arc partants du sommet courant
@param[in] entier ( Taille de la liste des arc partants du sommet courant )
@param[out] -
*/
void CSommet::SSetTaillePartant(int nArg)
{
	nSTaillePartant = nArg;
}
/**
@brief Getter pour la taille de la liste des arc partants du sommet courant
@param[in]  -
@param[out] entier ( Taille de la liste des arc partants du sommet courant )
*/
int CSommet::SGetTaillePartant()
{
	return nSTaillePartant;
}
/**
@brief Getter pour la liste des arc arrivants au sommet courant
@param[in]  -
@param[out] pointeur de liste de pointeurs CArcs* des arc arrivants au sommet courant
*/
CArc ** CSommet::SGetListeArrivant()
{
	return pASArrivant;
}
/**
@brief Setter pour la liste des arc arrivants au sommet courant
@param[in]  pointeur de liste de pointeurs CArcs* des arc arrivants au sommet courant
@param[in]  entier pour la taille de la liste de pointeurs CArcs* des arc arrivants saisie en argument au sommet courant
@param[out]  -
*/
void CSommet::SSetListeArrivant(CArc ** pAArg, int nTailleArrivant)
{
	pASArrivant = new CArc*[nTailleArrivant];
	for (int i = 0; i < nTailleArrivant; ++i) {
		pASArrivant[i] = new CArc(pAArg[i]);
	}
}
/**
@brief Getter pour la liste des arc partants du sommet courant
@param[in]  -
@param[out] pointeur de liste de pointeurs CArcs* des arc partants du sommet courant
*/
CArc ** CSommet::SGetListePartant()
{
	return pASPartant;
}
/**
@brief Setter pour la liste des arc partants du sommet courant
@param[in]  pointeur de liste de pointeurs CArcs* des arc partants du sommet courant
@param[in]  entier pour la taille de la liste de pointeurs CArcs* des arc partants saisie en argument au sommet courant
@param[out]  -
*/
void CSommet::SSetListePartant(CArc ** pAArg, int nTaillePartant)
{
	pASPartant = new CArc*[nTaillePartant];
	for (int i = 0; i < nTaillePartant; ++i) {
		pASPartant[i] = new CArc(pAArg[i]);
	}
}
/**
@brief Determiner si l'ID de sommet saisi est dans la liste des arcs partants
@param[in] Entier pour l'Id du sommet en question
@param[out] valeur booleenne indiquant si le sommet est deja existant dans la liste des arcs partants  ou non
*/
bool CSommet::SEstUnArcPartant(int nIdSommet)
{
	for (int i = 0; i < nSTaillePartant; i++)
	{
		if (pASPartant[i]->AGetDestination() == nIdSommet)
		{
			return true;
		}
	}
	return false;
}
/**
@brief Determiner si l'ID de sommet saisi est dans la liste des arcs arrivants
@param[in] Entier pour l'Id du sommet en question
@param[out] valeur booleenne indiquant si le sommet est deja existant dans la liste des arcs arrivants  ou non
*/
bool CSommet::SEstUnArcArrivant(int nIdSommet)
{
	for (int i = 0; i < nSTailleArrivant; i++)
	{
		if (pASArrivant[i]->AGetDestination() == nIdSommet)
		{
			return true;
		}
	}
	return false;
}
/**
@brief Ajouter un arc arrivant au sommet courant
@param[in] Entier pour l'Id du sommet en question
@param[out] -
*/
void CSommet::SAjouterArrivant(int nArg)
{
	if (!SEstUnArcArrivant(nArg))
	{
		++nSTailleArrivant;
		pASArrivant = (CArc**)realloc(pASArrivant, nSTailleArrivant * sizeof(CArc*));
		pASArrivant[nSTailleArrivant - 1] = new CArc();
		pASArrivant[nSTailleArrivant - 1]->ASetDestination(nArg);
	}
	else
	{
		CExceptionDuplicatedSommetId* EDSObjet = new CExceptionDuplicatedSommetId();
		EDSObjet->EModifierCodeErreur(1);
		throw(EDSObjet);
	}

}
/**
@brief Ajouter un arc partant du sommet courant
@param[in] Entier pour l'Id du sommet en question
@param[out] -
*/
void CSommet::SAjouterPartant(int nArg)
{
	if(!SEstUnArcPartant(nArg))
	{
		++nSTaillePartant;
		pASPartant = (CArc**)realloc(pASPartant, nSTaillePartant * sizeof(CArc*));
		pASPartant[nSTaillePartant - 1] = new CArc();
		pASPartant[nSTaillePartant - 1]->ASetDestination(nArg);
	}
	else
	{
	CExceptionDuplicatedSommetId* EDSObjet = new CExceptionDuplicatedSommetId();
	EDSObjet->EModifierCodeErreur(1);
	throw(EDSObjet);
    }
}
/**
@brief Supprimer un arc arrivant au sommet courant
@param[in] Entier pour l'Id du sommet en question
@param[out] -
*/
void CSommet::SSuprimerArrivant(int nNumero)
{
	if (nSTailleArrivant > 0)
	{
		for (int i = 0; i < nSTailleArrivant; i++)
		{
			if (pASArrivant[i]->AGetDestination() == nNumero)
			{
				if (i < (nSTailleArrivant - 1))
				{
					for (int j = i; j < nSTailleArrivant - 1; j++)
					{
						std::cout << pASArrivant[j]->AGetDestination() << " <- " << pASArrivant[j + 1]->AGetDestination() << std::endl;
						pASArrivant[j]->ASetDestination(pASArrivant[j + 1]->AGetDestination());
					}
				}
			}

		}
		--nSTailleArrivant;
	}
	else std::cout << "Suppression dans liste partant invalide" << std::endl;
}
/**
@brief Supprimer un arc partant du sommet courant
@param[in] Entier pour l'Id du sommet en question
@param[out] -
*/
void CSommet::SSuprimerPartant(int nNumero)
{
	if (nSTaillePartant > 0)
	{
		for (int i = 0; i < nSTaillePartant; i++)
		{
			if (pASPartant[i]->AGetDestination() == nNumero)
			{
				if (i < (nSTaillePartant - 1))
				{
					for (int j = i; j < nSTaillePartant - 1; j++)
					{
						pASPartant[j]->ASetDestination(pASPartant[j + 1]->AGetDestination());
					}
				}
			}

		}
		--nSTaillePartant;
		pASArrivant = (CArc**)realloc(pASArrivant, nSTailleArrivant * sizeof(CArc*));
	}
	else std::cout << "Suppression dans liste partant invalide" << std::endl;
}
/**
@brief Affichage du sommet courant
@param[in] -
@param[out] -
*/
void CSommet::SAfficherArcs()
{
	if(nSTaillePartant>0) 
	{
			for (int i = 0; i < nSTaillePartant; i++)
			{
				std::cout << nSNumero<<" -> "<< pASPartant[i]->AGetDestination()<< std::endl;
			}
	}
	if (nSTailleArrivant> 0) 
	{
		for (int i = 0; i < nSTailleArrivant; i++)
		{
			std::cout << nSNumero << " <- "<< pASArrivant[i]->AGetDestination() <<std::endl;
		}
	}
}
/**
@brief Supprimer un arc de la liste partante et de la liste arrivante ne meme temps 
@param[in] Entier pour l'Id du sommet en question
@param[out] -
*/
void CSommet::SSupprimerPartantArrivant(int nIdSommet)
{
	if(SEstUnArcPartant(nIdSommet)) SSuprimerPartant(nIdSommet);
	if(SEstUnArcArrivant(nIdSommet)) SSuprimerArrivant(nIdSommet);
}


