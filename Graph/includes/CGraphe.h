#ifndef C_GRAPHE_H
#define C_GRAPHE_H

#include <iostream>
#include "CSommet.h"

class CGraphe
{
private:
	CSommet** pSListeSommets;

	int nGTaille;


public:
	 CGraphe();

	 CGraphe(CGraphe* pGArg);

	 ~CGraphe();

	void GSetListeSommets(CSommet** pSArg,int nTaille);

	CSommet** GGetListeSommets();

	void GAddSommet(CSommet* pSArg);

	CSommet* GGetSommet(int nIdSommet);

	int GGetTaille();

	void GDeleteSommet(int nNumero);

	void GRemplacerSommet(int nNumero, CSommet* pSNewSommet);

	void GAfficherGraphe();

	bool GEstUnSommetExistant(int nIdSommet);

	bool GIdSommetsRepetes(CSommet** pSArg, int nTaille);

	CGraphe* GGrapheInverse();

};
#include "CGraphe.cpp"
#endif
