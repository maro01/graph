/**
@brief Constructeur CGrapheFichier
*/
CGrapheFichier::CGrapheFichier()
{
	pcGFFilePath = NULL;
}

/**
@brief Constructeur de recopie CGrapheFichier
*/
CGrapheFichier::CGrapheFichier(CGrapheFichier * pGFArg)
{
	pcGFFilePath = pGFArg->GFGetFilePath();
}

/**
@brief Constructeur CGrapheFichier avec le chemin en parametre
*/
CGrapheFichier::CGrapheFichier(char * pcArg)
{
	pcGFFilePath = pcArg;
}

/**
@brief Destructeur CGrapheFichier
*/
CGrapheFichier::~CGrapheFichier()
{
	free(pcGFFilePath);
}

/**
@brief Setter du chemin vers le fichier
@param[in] char* pcArg chaine de charactere correspondant aux chemin vers le fichier
*/
void CGrapheFichier::GFSetFilePath(char * pcArg)
{
	pcGFFilePath = pcArg;
}

/**
@brief Getter du chemin vers le fichier
@param[out] chaine de charactere correspondant aux chemin vers le fichier
*/
char * CGrapheFichier::GFGetFilePath()
{
	return pcGFFilePath;
}

/**
@brief Fonction principale construisant un pointeur vers un CGraphe construit à partir du fichier au chemin this.pcGFFilePath
@param[out] pointeur sur CGraphe construit à partir du fichier au chemin this.pcGFFilePath
*/
CGraphe * CGrapheFichier::GFGetGraphe()
{
	if(!pcGFFilePath)
	{
		CExceptionFileNotFound* EFNFException = new CExceptionFileNotFound();
		EFNFException->EModifierCodeErreur(1);
		throw EFNFException;
	}

	std::ifstream fichier(pcGFFilePath); //ouverture du fichier .txt 

	if (!fichier){
		CExceptionFileNotFound* EFNFException = new CExceptionFileNotFound();
		EFNFException->EModifierCodeErreur(1);
		throw EFNFException;
	}

	if (fichier)
	{
		char* cLine = new char[2048];
		fichier.getline(cLine, 2048);
		char* cNbSommets = new char[2048];
		strcpy_s(cNbSommets, sizeof(cLine + 10), cLine + 10); 	//on sépare la deuxieme ligne en 2 parties
		cLine[10] = '\0';								//cline servira a verifier la syntaxe et l'autre partie pour le nombre de sommet
		for (int nBoucle = 0; cLine[nBoucle]; nBoucle++)
		{										//lowercase pour la flexibilité
			cLine[nBoucle] = tolower(cLine[nBoucle]);
		}

		if (!strcmp(cLine, "nbsommets="))
		{
			fichier.getline(cLine, 2048);
			char* cNbArc = new char[2048]; 				//idem pour le nombre d'arc
			strcpy_s(cNbArc, sizeof(cLine + 7), cLine + 7);
			cLine[7] = '\0';

			for (int nBoucle = 0; cLine[nBoucle]; nBoucle++)
			{
				cLine[nBoucle] = tolower(cLine[nBoucle]);
			}

			if (!strcmp(cLine, "nbarcs="))
			{
				int nNbSommets = atoi(cNbSommets);
				int nNbArc = atoi(cNbArc);
				if(nNbSommets >= 0 && nNbArc >= 0)
				{
					fichier.getline(cLine, 2048);
					for (int nBoucle = 0; cLine[nBoucle]; nBoucle++)
					{
						cLine[nBoucle] = tolower(cLine[nBoucle]);
					}

					if (!strcmp(cLine, "sommets=["))
					{
						CGraphe* Ggraph = new CGraphe();					//creation du graphe
						char* cNumSommet = new char[2048];
						for(int nBoucle = 0; nBoucle < nNbSommets; nBoucle++) 	//creation des sommets
						{
							fichier.getline(cLine, 2048);
							strcpy_s(cNumSommet, sizeof(cLine + 7), cLine + 7);
							cLine[7] = '\0';
							for (int nBoucle = 0; cLine[nBoucle]; nBoucle++)
							{
								cLine[nBoucle] = tolower(cLine[nBoucle]);
							}
							if(!strcmp(cLine, "numero="))
							{
								CSommet* pSsommet = new CSommet();
								pSsommet->SSetNumero(atoi(cNumSommet));
								Ggraph->GAddSommet(pSsommet);
							}
						}										//les sommets ont été créés et ajouté au graphe Ggraph
						fichier.getline(cLine, 2048);
						if (!strcmp(cLine, "]"))
						{
							fichier.getline(cLine, 2048);
							for (int nBoucle = 0; cLine[nBoucle]; nBoucle++)
							{
								cLine[nBoucle] = tolower(cLine[nBoucle]);
							}
							if (!strcmp(cLine, "arcs=["))
							{
								char* cContext = new char[2048];
								char* cIdDebut = new char[2048];
								char* cIdFin = new char[2048];

								for(int nBoucle = 0; nBoucle < nNbArc; nBoucle++)
								{
									fichier.getline(cLine, 2048);
									strtok_s(cLine, ",", &cContext); 						// separation de debut et de fin

									strcpy_s(cIdDebut, sizeof(cLine + 6), cLine + 6);			//recuperation de l'id du sommet de depart
									cLine[6] = '\0';
									strcpy_s(cIdFin, sizeof(cContext + 5), cContext + 5);			//recuperation de l'id du sommet d'arrivé
									cContext[5] = '\0';
									int nIdDebut = atoi(cIdDebut);
									int nIdFin = atoi(cIdFin);

									if (!strcmp(cLine, "Debut=") && !strcmp(cContext, " Fin=")) 	// verification des balises
									{
										if(Ggraph->GEstUnSommetExistant(nIdDebut) && Ggraph->GEstUnSommetExistant(nIdFin))
										{	//creation des 2 arc arrivant et partant correspondant a l'arc reel

											Ggraph->GGetSommet(nIdDebut)->SAjouterPartant(nIdFin);
											Ggraph->GGetSommet(nIdFin)->SAjouterArrivant(nIdDebut);
										}
									}
								}
								fichier.getline(cLine, 2048);
								if (!strcmp(cLine, "]")){
									return Ggraph;
								}
							}
						}
					}
				}
			}
		}

	}
	CExceptionFileSyntax* EFSException = new CExceptionFileSyntax();
	EFSException->EModifierCodeErreur(1);
	throw(EFSException);
}
