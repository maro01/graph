
/**
@brief Constructeur par defaut d'objet CExceptionFileNotFound
*/
CExceptionFileNotFound::CExceptionFileNotFound() : CException()
{
	pcEFNFMessage = (char*)malloc(sizeof(char) * strlen("CExceptionFileNotFound raised") + 1);
	strcpy_s(pcEFNFMessage, sizeof("CExceptionFileNotFound raised"), "CExceptionFileNotFound raised");
}

/**
@brief Constructeur de copie d'objet pointeur CExceptionFileNotFound*
@param[in] Objet pointeur CExceptionFileNotFound*
*/
CExceptionFileNotFound::CExceptionFileNotFound(CExceptionFileNotFound* pEFNFArg) : CException(pEFNFArg)
{
	pcEFNFMessage = pEFNFArg->EFNFGetMessage();
}
/**
@brief Destructeur d'objet CExceptionFileNotFound
*/
CExceptionFileNotFound::~CExceptionFileNotFound()
{
	free(pcEFNFMessage);
}

/**
@brief Affichage d'erreur d'objet CExceptionFileNotFound
@param[in] -
@param[out] -
*/
void CExceptionFileNotFound::EAfficherErreur()
{
	if (EGetCodeErreur() == 1) std::cerr<<"\n\n"<<pcEFNFMessage<<"\n\n";
	else std::cerr<<"\n\nCExceptionFileNotFound not raised\n\n";
}
/**
@brief Setter du message d'erreur d'objet CExceptionFileNotFound
@param[in]  Pointeur de chaine de caracteres
@param[out] -
*/
void CExceptionFileNotFound::EFNFSetMessage(char* pcMessage)
{
	strcpy_s(pcEFNFMessage, sizeof(pcMessage), pcMessage);
}
/**
@brief Getter du message d'erreur d'objet CExceptionFileNotFound
@param[in]  -
@param[out] -
*/
char * CExceptionFileNotFound::EFNFGetMessage()
{
	return pcEFNFMessage;
}
