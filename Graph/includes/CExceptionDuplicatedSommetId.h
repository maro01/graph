#ifndef C_EXCEPTION_DUPLICATED_SOMMET_ID_H
#define C_EXCEPTION_DUPLICATED_SOMMET_ID_H

#include <iostream>

#include "CException.h"

class CExceptionDuplicatedSommetId : public CException
{
private:

	char* pcEDSMessage;

public:
	 CExceptionDuplicatedSommetId();

	 CExceptionDuplicatedSommetId(CExceptionDuplicatedSommetId* pEDSIArg);

	 ~CExceptionDuplicatedSommetId();

	 void EAfficherErreur();

	void EDSISetMessage(char* pcMessage);

	char* EDSIGetMessage();

};
#include "CExceptionDuplicatedSommetId.cpp"
#endif
