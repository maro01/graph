#ifndef C_SOMMET_H
#define C_SOMMET_H

#include <iostream>

#include "CArc.h"
#include "CExceptionDuplicatedSommetId.h"

static unsigned int idSommet = 0;

class CSommet
{
private:
	int nSNumero;

	CArc** pASArrivant;

	CArc** pASPartant;

	int nSTailleArrivant;

	int nSTaillePartant;

public:

	 CSommet();

	 CSommet(CSommet* pSArg);

	 ~CSommet();

	int SGetNumero();

	void SSetNumero(int nArg);

	void SSetTailleArrivant(int nArg);

	int SGetTailleArrivant();

	void SSetTaillePartant(int nArg);

	int SGetTaillePartant();

	CArc** SGetListeArrivant();

	void SSetListeArrivant(CArc** pAArg, int nTailleArrivant);

	CArc** SGetListePartant();

	void SSetListePartant(CArc** pAArg, int nTaillePartant);

	bool SEstUnArcPartant(int nIdSommet);

	bool SEstUnArcArrivant(int nIdSommet);

	void SAjouterArrivant(int nArg);

	void SSuprimerArrivant(int nNumero);

	void SAjouterPartant(int nArg);

	void SSuprimerPartant(int nNumero);

	void SAfficherArcs();

	void SSupprimerPartantArrivant(int nIdSommet);

};
#include "CSommet.cpp"
#endif
