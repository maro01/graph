#include <iostream>
#include "includes/CGraphe.h"
#include "includes/CSommet.h"
#include "includes/CArc.h"
#include "includes/CGrapheFichier.h"
#include "includes/CExceptionDuplicatedSommetId.h"
#include "includes/CExceptionFileNotFound.h"
#include "includes/CExceptionFileSyntax.h"

int main(int argc, char *argv[])
{
	if (!argv[1]) //on verifie qu'il y a bien un fichier pass� en param�tre
	{
		std::cout << "Veuillez fournir un chemin vers le fichier en parametre" << std::endl;
		return 0;
	}

	try
	{
		/* Pour un nom de fichier pass� en param�tre, lire le fichier et cr�er le graphe associ� */
		CGrapheFichier* fichier = new CGrapheFichier();
		fichier->GFSetFilePath(argv[1]);
		CGraphe* Ggraph = fichier->GFGetGraphe();

		/* Afficher le graphe � l��cran */
		Ggraph->GAfficherGraphe();

		/* Inverser tous les arcs du graphe pour obtenir un nouveau graphe */
		CGraphe* GgraphInverse = Ggraph->GGrapheInverse();

		/* Afficher ce nouveau graphe � l��cran */
		GgraphInverse->GAfficherGraphe();

		delete(Ggraph);
		delete(GgraphInverse);
	}
	catch (CExceptionFileNotFound* EFNFObj)
	{
		EFNFObj->EAfficherErreur();
	}
	catch (CExceptionFileSyntax* EFSObj)
	{
		EFSObj->EAfficherErreur();
	}
	catch (CExceptionDuplicatedSommetId* EDSIObj)
	{
		EDSIObj->EAfficherErreur();
	}
	return 0;


			/* Demonstration des fonctions de la librairie (� decommenter) */
	///** Creation et initialisation d'un nouveau sommet*/
	//CSommet *pSsommet1 = new CSommet();

	///** Creation et initialisation de 6 arcs */
	//CArc *pAarc1 = new CArc(1);
	//CArc *pAarc2 = new CArc(2);
	//CArc *pAarc3 = new CArc(3);
	//CArc *pAarc4 = new CArc(4);
	//CArc *pAarc5 = new CArc(5);
	//CArc *pAarc6 = new CArc(6);

	//try
	//{
	//	/** Ajout des arcs au sommet 1 */
	//	pSsommet1->SAjouterPartant(pAarc2->AGetDestination()); // partir de sommet 1 au sommet 2
	//	pSsommet1->SAjouterArrivant(pAarc3->AGetDestination());  // venir de sommet 3 au sommet 1 
	//	pSsommet1->SAjouterArrivant(pAarc4->AGetDestination());  // venir de sommet 4 au sommet 1
	//	pSsommet1->SAjouterPartant(pAarc5->AGetDestination()); // partir de sommet 1 au sommet 5
	//}
	//catch (CExceptionDuplicatedSommetId* EDSObj)
	//{
	//	/** Catching et affichage d'erreur contoure*/
	//	EDSObj->EAfficherErreur();
	//}


	///** Creation et initialisation d'un nouveau sommet*/
	//CSommet *pSsommet2 = new CSommet();

	//try
	//{
	//	/** Ajout des arcs au sommet 2 */
	//pSsommet2->SAjouterPartant(pAarc1->AGetDestination()); // partir de sommet 2 au sommet 1
	//pSsommet2->SAjouterPartant(pAarc3->AGetDestination());  // partir de sommet 2 au sommet 3
	//pSsommet2->SAjouterArrivant(pAarc4->AGetDestination());  // venir de sommet 4 au sommet 2
	//pSsommet2->SAjouterArrivant(pAarc5->AGetDestination()); // venir de sommet 5 au sommet 2
	//pSsommet2->SAjouterArrivant(pAarc6->AGetDestination()); // venir de sommet 6 au sommet 2
	//}
	//catch (CExceptionDuplicatedSommetId* EDSObj)
	//{
	//	/** Catching et affichage d'erreur contoure*/
	//	EDSObj->EAfficherErreur();
	//}

	///** Creation et initialisation d'un nouveau sommet*/
	//CSommet *pSsommet3 = new CSommet();

	//try
	//{
	//	/** Ajout des arcs au sommet 3 */
	//	pSsommet3->SAjouterArrivant(pAarc1->AGetDestination()); // venir de sommet 1 au sommet 3
	//	pSsommet3->SAjouterPartant(pAarc2->AGetDestination());  // partir de sommet 3 au sommet 2
	//	pSsommet3->SAjouterArrivant(pAarc5->AGetDestination()); // venir de sommet 5 au sommet 3
	//	pSsommet3->SAjouterArrivant(pAarc6->AGetDestination()); // venir de sommet 6 au sommet 3
	//}
	//catch (CExceptionDuplicatedSommetId* EDSObj)
	//{
	//	/** Catching et affichage d'erreur contoure*/
	//	EDSObj->EAfficherErreur();
	//}

	///** Creation et initialisation d'un nouveau sommet*/
	//CSommet *pSsommet4 = new CSommet();

	//try
	//{
	//	/** Ajout des arcs au sommet 4 */
	//	pSsommet4->SAjouterArrivant(pAarc1->AGetDestination()); // venir de sommet 1 au sommet 4
	//	pSsommet4->SAjouterPartant(pAarc2->AGetDestination());  // partir de sommet 4 au sommet 2
	//	pSsommet4->SAjouterArrivant(pAarc6->AGetDestination()); // venir de sommet 6 au sommet 4
	//}
	//catch (CExceptionDuplicatedSommetId* EDSObj)
	//{
	//	/** Catching et affichage d'erreur contoure*/
	//	EDSObj->EAfficherErreur();
	//}

	///** Creation et initialisation d'un nouveau sommet*/
	//CSommet *pSsommet5 = new CSommet();

	//try
	//{
	//	/** Ajout des arcs au sommet 5 */
	//	pSsommet5->SAjouterPartant(pAarc1->AGetDestination()); // partir de sommet 5 au sommet 1
	//}
	//catch (CExceptionDuplicatedSommetId* EDSObj)
	//{
	//	/** Catching et affichage d'erreur contoure*/
	//	EDSObj->EAfficherErreur();
	//}

	///** Creation et initialisation d'un nouveau sommet*/
	//CSommet *pSsommet6 = new CSommet();  // ne part � aucun sommet



	///** creer un graphe de sommets */
	//CGraphe Ggraph = new CGraphe();

	///** ajout des sommets */
	//Ggraph.GAddSommet(pSsommet1);
	//Ggraph.GAddSommet(pSsommet2);
	//Ggraph.GAddSommet(pSsommet3);
	//Ggraph.GAddSommet(pSsommet4);
	//Ggraph.GAddSommet(pSsommet5);
	//Ggraph.GAddSommet(pSsommet6);

	///** affichage du graph */
	//Ggraph.GAfficherGraphe();
	//Ggraph.GDeleteSommet(3);
	//Ggraph.GAfficherGraphe();
}

